# README #

JWatch saves the progress of TV series you are watching, and let's you start where you've left off.

Depends on [mpv](http://mpv.io/) and java.

### Usage ###

JWatch is mostly intended to be used with the command line or a launcher such as dmenu.

usage: java -jar JWatch.jar [series] [episode] [arguments]

This can be simplified with a bash script, for example


```
#!/bin/bash
cd ~/.bin/jwatch
java -jar JWatch.jar "$@" 

```


[series] does not have to be the whole name for example "cards" would be enough for "House of Cards" as long as no other series starts with "cards".

[episode] has to match the regular expression to detect season/episode identifiers, by default for example s01e01 or 01x01

If no arguments are given it will start the next episode from the last watched TV series.

Arguments:

* -h, --help:	prints help 

* --reset:	start series at s01e00 or s01e01 if e00 not available

* --prev:	play previous episode if available

* --rnd:	play the series on shuffle without saving progress

* --stop:	stop after current episode (has to be last argument given)

* --shutdown:	shutdown pc after current episode(has to be last argument given)

* --debug:	output debug information


other options will be passed directly to mpv.
Format has to be --flag=value or just --flag


### Config ###

JWatch creates a config file in ~/.config/JWatch

config options (all optional):

 
* screen=<Integer>

* subtitles=<true/false>

* episode_regex=<regular expression> (Regular expression to detect season/episode identifiers (e.g. s01e01))

* dir=<root dir of series>

### Windows usage ###

If you are using windows the [mpv](http://mpv.io/) folder has to be in the same directory as the JWatch.jar file. Not all options may be working (e.g. shutdown).