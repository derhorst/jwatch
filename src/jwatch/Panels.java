package jwatch;

import javax.swing.*;
import java.awt.*;;


public class Panels extends JPanel {

	private static final long serialVersionUID = 1L;
	static JFileChooser chooser;
	static String choosertitle = "Select the root directory of your series";
   

	public static String actionPerformed() {
		    
		chooser = new JFileChooser(); 
		chooser.setCurrentDirectory(new java.io.File("/"));
		chooser.setDialogTitle(choosertitle);
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		
		// disable the "All files" option.
		chooser.setAcceptAllFileFilterUsed(false);

		if (chooser.showSaveDialog(chooser) == JFileChooser.APPROVE_OPTION) { 
		// chooser.getCurrentDirectory());
			return chooser.getSelectedFile().toString();
		}
		else {
			System.out.println("No Selection ");
		}
	return null;
	}
	
	// panel to choose series if none is given
	static void seriesInput() {
        JComboBox<String> combo = new JComboBox<String>();
        // populate combobox
        for(String s : Main.seriesNames){
        	combo.addItem(s);
        }
        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.add(new JLabel("Series to watch:"));
        panel.add(combo);
        int result = JOptionPane.showConfirmDialog(null, panel, "Select series to watch",
            JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
        	Main.series = combo.getSelectedItem().toString();
        } else {
            System.out.println("Cancelled");
        	Main.play = false;
        }
    }

}