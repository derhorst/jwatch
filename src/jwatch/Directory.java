package jwatch;

import java.io.File;
import java.util.ArrayList;

public class Directory {
	public static void getDirs(String directoryName, ArrayList<String> dirs) {
		File directory = new File(directoryName);
		File[] fList = directory.listFiles();
		 for (File file : fList) {
		        if (file.isDirectory()) {
		            dirs.add(file.toString().substring(Main.dir.length()+1));
		        } 
		    }
	}
	
	public static void listf(String directoryName, ArrayList<String> files, String series) throws Exception {
	    File directory = new File(directoryName);
	    if(!directory.exists()){
	    	throw new Exception("The directory given in jwatch.conf does not exist!\n");
	    }
	    // get all the files from a directory
	    File[] fList = directory.listFiles();
	    for (File file : fList) {
	        if (file.isFile() && !files.contains(file.toString())) {
	            files.add(file.toString());
	        } else if (file.isDirectory() 
	        		&& (file.toString().substring(Main.dir.length()).equals(File.separator + series)
	        		|| file.toString().substring(Main.dir.length()).contains(File.separator + series + File.separator))) {
	            listf(file.getAbsolutePath(), files, series);
	        }
	    }
	}
}
