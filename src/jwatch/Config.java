package jwatch;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class Config {
	private static String dir = Main.home + "/.config/JWatch/JWatch.conf";
	
	public static void save(String series, String directory) throws IOException{
		Properties p = load();
		p.setProperty(series, directory);
		FileOutputStream stream = new FileOutputStream(dir);
		p.store(stream, dir);
		
	}
	 
public static Properties load(){
	if(!new File(dir).exists()){
		try {
			 createConfig();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	Properties p = new Properties();
	if(Main.play){
		try {
			p.load(new FileInputStream(dir));
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return p;
	}
	return null;
	
}
	 
	private static void createConfig() throws IOException {
		String rootdir = null;
		if(rootdir==null){
			rootdir = Panels.actionPerformed();
		}
		if(rootdir != null){
			File config = new File(dir);
			config.getParentFile().mkdirs();
			config.createNewFile();

			System.out.println("Please select the root directory for your series");
			
			Main.dir = rootdir;
			save("dir", rootdir);
			save("episode_regex", "[sS]?(\\d+)[eExX](\\d{2}).*");
			save("screen","0");
			save("subtitles","true");
		}else{
			System.out.println("No root directory given. Exiting.");
			Main.play = false;
		}
		
	}

	public static void remove(String key){
		 try {
            File fileDir = new File(dir);
            Properties properties = new Properties();
            properties.load(new FileInputStream(fileDir));
            properties.remove(key);
            OutputStream out = new FileOutputStream(fileDir);
            properties.store(out, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
	 }
}
