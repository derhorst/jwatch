package jwatch;
import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class SeasonsComparator implements Comparator<String> {

    /* (non-Javadoc)
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(String arg0, String arg1) {
    	//return +1 if arg0 > arg1
        //return 0 if arg0 == arg1
        //return -1 if arg0 < arg1
    	String dir = Config.load().getProperty("dir");
    	arg0 = arg0.substring(dir.length()+1);
    	arg1 = arg1.substring(dir.length()+1);
    	// checking for the series name
    	int c = arg0.substring(0,arg0.indexOf(File.separator)).compareTo(arg1.substring(0,arg1.indexOf(File.separator)));
    	if(c>0){
    		return 1;
    	}else if (c<0){
    		return -1;
    	}
    	
    	// get the last directories without files
    	String match0 = arg0.substring(0, arg0.lastIndexOf(File.separator));
		String match1 = arg1.substring(0, arg1.lastIndexOf(File.separator));
    	
    	// extract season numbers and compare them
    	if(new File(dir + File.separator + match0).isDirectory() && 
    			new File(dir + File.separator + match1).isDirectory()){
    		
    		// pattern to detect integers
    		final Pattern pattern = Pattern.compile("\\d+"); // the regex
    		final Matcher matcher0 = pattern.matcher(match0);
    		final Matcher matcher1 = pattern.matcher(match1);

    		final ArrayList<Integer> ints0 = new ArrayList<Integer>();
    		final ArrayList<Integer> ints1 = new ArrayList<Integer>();
    		
    		// find all integers in arg0 and arg1
    		while (matcher0.find()) {
    		    ints0.add(Integer.parseInt(matcher0.group()));
    		}
    		
    		while (matcher1.find()) {
    		    ints1.add(Integer.parseInt(matcher1.group()));
    		}
    		
    		int iArg0 = -1;
    		int iArg1 = -1;
    		
    		// get the last int and use it as season identifier
    		if(ints0.size()-1 >= 0){
    			iArg0 = ints0.get(ints0.size()-1);
    		}
    		
    		if(ints1.size()-1 >= 0){
    			iArg1 = ints1.get(ints1.size()-1);
    		}
        	
        	if(iArg0 > iArg1){
        		return 1;
        	}if(iArg0 < iArg1){
        		return -1;
        	} else {
        		// if the season matches compare the filenames
        		arg0 = arg0.substring(arg0.indexOf(File.separator)+1);
        		arg1 = arg1.substring(arg1.indexOf(File.separator)+1);
        		c = arg0.compareTo(arg1);
            	if(c>0){
            		return 1;
            	}else if (c<0){
            		return -1;
            	}else{
            		return 0;
            	}  	           	
        	}
    	}     
        return 0;
    }
}
