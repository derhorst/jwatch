package jwatch;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Main {
	static boolean play = true;
	static String home = System.getProperty("user.home");
	static ArrayList<String> files = new ArrayList<String>();
	static ArrayList<String> seriesNames = new ArrayList<String>();;
	static String curr;
	static String series;
	static Properties config = Config.load();
	static String dir;
	static String screen;
	private static ArrayList<String> command = new ArrayList<String>();
	private static String input;
	private static String watching;
	private static String episode;
	private static String episodeRegex = "[sS]?(\\d+)[eExX](\\d{2}).*";
	private static boolean prev = false;
	private static boolean reset = false;
	private static boolean shuffle = false;
	private static boolean debug = false;
	private static boolean subtitles = true;
	public static void main(String[] args) throws Exception {
		
		cleanup();
		
		// get args
		getArgs(0, args);
		
		if(play){
			// load config
			dir = config.getProperty("dir");
			screen = config.getProperty("screen");
			subtitles = Boolean.valueOf(config.getProperty("subtitles"));
			watching = config.getProperty("watching");
			// if there is a regex in the config given use that
			if(config.getProperty("episode_regex")!= null){
				episodeRegex = config.getProperty("episode_regex");
			}
			
			// after config is loaded prepare commands before getting args
			prepareCommands();

			// get the names of all the series
			Directory.getDirs(dir, seriesNames);
			
			if(input != null){
				series = getName(input);
				if(debug){
					System.out.println("Name form input: " + series);
				}
			}else{
				series = watching;
			}
			
			// get the user to pick series
			if(series==null || series.equals("")){
				Panels.seriesInput();
		    }
			
			// if no matching series is found throw an exception
			if(series==null){
				 throw new Exception("No matching series found.");
			}
			
			// populate files
			Directory.listf(dir + File.separator + series, files, series);
			
			// if there is an episode identifier try to get the matching episode
			if(episode != null){
				curr = getEpisode();
			}
			
			// load the current episode of the specified series, if empty get the first episode
			if(curr==null){
				curr = Config.load().getProperty(series);
				
				// curr loaded but file does not exist
				if(curr != null && !new File(dir + curr).exists()){
					Pattern pattern = Pattern.compile(episodeRegex);
					Matcher matcher = pattern.matcher(curr);
					
					// if the pattern finds both season and episode get the correct file
					// if non is found reset the series
					if (matcher.find() && !matcher.group(2).isEmpty())
					{
					    episode="s"+matcher.group(1)+"e"+matcher.group(2);
					    curr = getEpisode();				    
					}else{
						curr=null;
					}
				}
			}
			
			// get previous file
			if(reset || curr==null || curr.equals("")){
				curr = getFirst(series);
			}
			
			if(prev){
				curr = getPrev();
			}
			
			
			if(debug){		
				System.out.println("dir: " + dir);
				System.out.println("current: " + curr);
				System.out.println("#series: " + seriesNames.size());
				System.out.println("watching: " + watching);
				System.out.println("input: " + input);
				System.out.println("series: " + series);
				System.out.println("\ngiven arguments: " + args.length);
				for(int i=0; i<args.length; i++){
					System.out.println(args[i]);
				}		
				if(config.getProperty("episode_regex")!= null){
					System.out.println("use episode regex from config: " + episodeRegex);
				}
			}

	
			// Kill open players and start playback
			// killPlayer();
			
			
			// sorting the files with an season numbers aware comparator
			Collections.sort(files, new SeasonsComparator());
			
			//curr = Config.load().getProperty(series);
			
			// throw exception if neither current nor first episode is found
			if(curr==null){
				throw new Exception("Neither current nor first episode was found. You can specify the starting manually.");
			}
			if(play){
				play();
			}	
		}
	}
	
	private static void prepareCommands() {
		String OS = System.getProperty("os.name").toLowerCase();
		if(OS.indexOf("win") >= 0){
			command.add(0, "mpv\\mpv.exe");
			command.add("--config-dir=./mpv/");
		}else{
			command.add(0, "mpv");
		}
		if(!subtitles){
			command.add("--no-sub");
		}
		if(screen!=null){
			command.add("--screen="+screen);
		}		
		command.add("--playlist=" + home + "/.config/JWatch/.tmp.m3u");
		command.add("--autofit=100%x100%");
		if(shuffle){
			command.add("--shuffle");
		}
	}
	
	// get the name of the series from input
	static String getName(String input) throws Exception{
		if(debug){
			System.out.println("Get Name");
		}
		
		// first check if there's a series which starts with the given input, 
		// if that fails check if there is a series which contains the input anywhere in the name
		for(String s : seriesNames){
			if(s.toLowerCase().startsWith(input.toLowerCase())){
				return s;
			}
		}
		for(String s : seriesNames){
			if(s.toLowerCase().contains(input.toLowerCase())){
				return s;
			}
		}
		return null;
	}
	
	private static void play() throws IOException{
		// play the given playlist
		
		// prepare the playlist and command for playback according to the shuffle flag
		if(!shuffle){
			Playlist.create();
			Config.save("watching", series);
		}else{
			Playlist.createShuffle();
			
		}
		if(debug){
			System.out.println("mvp arguments: " + command.size());
			for(int i = 0; i< command.size(); i++){
				System.out.println(command.get(i));
			}
		}
		
		ProcessBuilder pb = new ProcessBuilder(command);
		pb.redirectErrorStream(true);
		
		Process proc = pb.start();

		// read the process's output
		String line;             
		BufferedReader in = new BufferedReader(new InputStreamReader(
		        proc.getInputStream())); 

		float hrs = 0;
		float min = 0;
		float tolHrs = 0;
		float tolMin = 0;
		String av = null;

		while ((line = in.readLine()) != null) {
			if(debug){
				if(!line.startsWith("AV") && !line.startsWith("(...)")){
					System.out.println(line);
				}
			}
			
			if(line.contains("Playing: ")) {
				
				hrs = 0;
				min = 0;
				tolHrs = 0;
				tolMin = 0;
				
				// if shuffle flag is not set save the progress made in the playlist
				System.out.println("Playing: "+line.substring(dir.length()+10));
				
				// check if the stop or shutdown file exist and act accordingly
				File stop = new File(home + "/.config/JWatch/stop");
				File shutdown = new File(home + "/.config/JWatch/shutdown");
				
				if(stop.exists() && !line.substring(dir.length()+9).equals(curr)){
					stop.delete();
					if(!shuffle){
						curr = line.substring(9);
						Config.save("watching", series);
						Config.save(series, curr.substring(dir.length()));
					}
					break;
				}else if(shutdown.exists() && !line.substring(dir.length()+9).equals(curr)){
					Runtime.getRuntime().exec("poweroff");
					shutdown.delete();
					if(!shuffle){
						curr = line.substring(9);
						Config.save("watching", series);
						Config.save(series, curr.substring(dir.length()));
					}
					break;
				}else if(!shuffle){
					curr = line.substring(9);
					Config.save("watching", series);
					Config.save(series, curr.substring(dir.length()));
				}
				
			// these conditions are only checked if the shuffle flag is not set
			}else if(line.startsWith("AV:") && !shuffle){
				av = line;
			}else if(line.contains("Exiting... (End of file)") && !shuffle){
				if(debug){
					System.out.println("remove series/watching");
				}
				Config.remove("watching");
				Config.remove(series);
			}else if(line.contains("Exiting... (Quit)") && !shuffle){
				
				hrs = Integer.parseInt(av.substring(4,6));
				min = Integer.parseInt(av.substring(7,9));
				
				tolHrs = Integer.parseInt(av.substring(15,17));
				tolMin = Integer.parseInt(av.substring(18,20));
				float perc = (hrs*60+min)/(tolHrs*60+tolMin);
				if(debug){
					System.out.println(perc);
					System.out.println(curr);
				}
				if(min > 0 && perc > 0.80){
					int i = files.indexOf(curr);
					
					if(files.get(i+1).contains(series)){
						Config.save(series, files.get(i+1).substring(dir.length()));
					}else{
						if(debug){
							System.out.println("Remove series from config");
						}
					}	
				}
			}
		}
		// clean-up	
		proc.destroy();
		
		Playlist.delete();
		
			  
	}

	// get the first episode of series
	private static String getFirst(String series) throws IOException{
		for(String s : files){
			  if(s.toLowerCase().contains(series.toLowerCase()) 
					  && (s.toLowerCase().contains("s01e01") 
					  || (s.toLowerCase().contains("s01e00")) 
					  || (s.toLowerCase().contains("1x01"))
					  || (s.toLowerCase().contains("1x00")))){
				  Config.save(series, s.substring(dir.length()));
				  return s.substring(dir.length());
			  }
			}
		if(files.get(0).toLowerCase().contains(series.toLowerCase())){
			System.out.println("No first episode could be detected! Playing first file.");
			Config.save(series, files.get(0).substring(dir.length()));
			return files.get(0).substring(dir.length());
		}
		return null;
	}
	
	// delete any stop/shutdown files already created
	private static void cleanup(){
		
		File stop = new File(home + "/.config/JWatch/stop");
		if(stop.exists()){	
			stop.delete();
		}
		
		File shutdown = new File(home + "/.config/JWatch/shutdown");
		if(shutdown.exists()){	
			shutdown.delete();
		}
	}
	
	private static String getEpisode() throws IOException{
		for(String s : files){
			if(s.toLowerCase().substring(dir.length(), s.lastIndexOf(File.separator)).contains(series.toLowerCase()) 
					&& s.toLowerCase().contains(episode)){
				Config.save(series, s.substring(dir.length()));
				return s.substring(dir.length());
			}
		}
		return null;
	}
	
	private static String getPrev() throws IOException{
		int i = 0;
		
		String current = config.getProperty(series);
		i = files.indexOf(dir + current);
		
		// get index of current file in filearray
		// if previous element is not from the same series do nothing because there is no previous file		
		if(i > 0){
			Config.save(series, files.get(i-1).substring(dir.length()));
			return files.get(i-1).substring(dir.length());
		}
		return current;
	}
	
	private static void getArgs(int arg, String args[]) throws Exception{		
		if(args.length>arg){	
			// if first argument given is not the name of a series set series = watching
			if(args[arg].equals("--stop")){
				if(args.length==1){
					File f = new File(home + "/.config/JWatch/stop");
					f.createNewFile();
					System.out.println("Stop file created.");
					play = false;
					return;
				}else if(arg+1==args.length){
					File f = new File(home + "/.config/JWatch/stop");
					f.createNewFile();
					return;
				}else{
					System.out.println("\nThe stop flag must be the last argument given.\nAborting!");
					play = false;
					return;
				}
				
			}else if(args[arg].equals("--shutdown")){
				if(args.length==1){
					File f = new File(home + "/.config/JWatch/shutdown");
					f.createNewFile();
					System.out.println("Shutdown file created.");
					play = false;
					return;
				}else if(arg+1==args.length){
					File f = new File(home + "/.config/JWatch/shutdown");
					f.createNewFile();
					return;
				}else{
					System.out.println("\nThe shutdown flag must be the last argument given.\nAborting!");
					play = false;
					return;
				}
			}else if(args[arg].equals("--debug")){
				debug = true;
				getArgs(arg+1, args);
			}else if(args[arg].equals("--rnd")){
				shuffle = true;
				getArgs(arg+1, args);
			}else if(args[arg].equals("--reset")){
				reset = true;
				getArgs(arg+1, args);
			}else if(args[arg].equals("--prev")){
				prev = true;
				getArgs(arg+1, args);
			}else if(args[arg].equals("-h") || args[arg].equals("--help")){
				Help.print();
				play = false;
				return;
			}else if(arg == 0 && !args[arg].startsWith("-") && !args[arg].matches(episodeRegex)){
				// while the argument has no dash and doesnt match the series identifier pattern (e.g. s01e03) add them to input
				while(arg < args.length && !args[arg].matches(episodeRegex) && !args[arg].startsWith("-")){
					if(input == null){
						input = args[arg];
					}else{
						input = input + " " + args[arg];
					}
					arg++;
				}
				input = input.trim();
		
				if(debug){
					System.out.println("Input: " + input);
				}
				series = getName(input);
				
				getArgs(arg, args);
			// set episode to string which matches regex for example s01e02.
			}else if(args[arg].matches(episodeRegex)){
				if(debug){
					System.out.println("episode identifier detected");
				}
				episode = args[arg];
				getArgs(arg+1, args);
			}else if(args.length > arg){
				command.add(args[arg]);
				getArgs(arg+1, args);
			}
		}else{
			return;
		}
	}
	
}
