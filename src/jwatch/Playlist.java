package jwatch;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.nio.file.Files;

public class Playlist {

	public static void delete(){
		File playlist = new File(Main.home + "/.config/JWatch/.tmp.m3u");
		if(playlist.exists()){	
			playlist.delete();
		}
	}
	
	static void create() throws FileNotFoundException{
		// building playlist
		// get start index of the while loop
		int index = Main.files.indexOf(Main.dir + Main.curr);
		PrintWriter out = new PrintWriter(Main.home + "/.config/JWatch/.tmp.m3u");
		// && Main.files.get(index).contains(Main.series)
		while(index < Main.files.size()){
			out.println(Main.files.get(index));
			index++;
		}
		out.close();
	}
	
	static void createShuffle() throws FileNotFoundException{
		// building playlist for shuffle
		PrintWriter out = new PrintWriter(Main.home + "/.config/JWatch/.tmp.m3u");
		for(String s : Main.files){
			if(s.toLowerCase().contains(Main.series.toLowerCase())){
				out.println(s);
			}
		}
		out.close();
	}
}
