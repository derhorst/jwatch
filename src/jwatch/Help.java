package jwatch;

public class Help {

	static void print() {
		System.out.println("usage: JWatch [series] [episode] [arguments]\n\nArguments:\n"
				+ "\n-h, --help:\tprints help "
				+ "\n--reset:\tstart series at s01e00 or s01e01 if e00 not available"
				+ "\n--prev:\t\tplay previous episode if available"
				+ "\n--rnd:\t\tplay the series on shuffle without saving progress"
				+ "\n--stop:\t\tstop after current episode (has to be last argument given)"
				+ "\n--shutdown:\tshutdown pc after current episode(has to be last argument given)"
				+ "\n--debug:\toutput debug information"
				+ "\n\nother options will be passed directly to mpv."
				+ "\nFormat has to be --flag=value or just --flag");
	}
}
